/*
 * l3gd20.h
 *
 *  Created on: 2 lut 2017
 *      Author: Olek
 */

#ifndef L3GD20_H_
#define L3GD20_H_

#include "stm32f3xx_hal.h"

// Gyroscope registers
#define WHO_AM_I 0x0F
#define CTRL_REG1 0x20
#define CTRL_REG2 0x21
#define CTRL_REG3 0x22
#define CTRL_REG4 0x23
#define CTRL_REG5 0x24
#define REFERENCE 0x25
#define OUT_TEMP 0x26
#define STATUS_REG 0x27
#define OUT_X_L 0x28
#define OUT_X_H 0x29
#define OUT_Y_L 0x2A
#define OUT_Y_H 0x2B
#define OUT_Z_L 0x2C
#define OUT_Z_H 0x2D
#define FIFO_CTRL_REG 0x2E
#define FIFO_STC_REG 0x2F
#define INT1_CFG 0x30
#define INT1_SRC 0x31
#define INT1_TSH_XH 0x32
#define INT1_TSH_XL 0x33
#define INT1_TSH_YH 0x34
#define INT1_TSH_YL 0x35
#define INT1_TSH_ZH 0x36
#define INT1_TSH_ZL 0x37
#define INT1_DURATION 0x38

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// GPIO Port and Pin of Slave Select
#define SS_PORT GPIOE
#define SS_PIN GPIO_PIN_3
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

typedef struct
{
	float x;
} l3gd20;

// Gyroscope functions

void Gyro_Write1Byte(SPI_HandleTypeDef *,uint8_t,uint8_t *);
void Gyro_WriteMultipleBytes(SPI_HandleTypeDef *,uint8_t,uint8_t *,int);
void Gyro_Read1Byte(SPI_HandleTypeDef *,uint8_t,uint8_t *);
void Gyro_ReadMultipleBytes(SPI_HandleTypeDef *,uint8_t,uint8_t *,int);

void Gyro_Init(SPI_HandleTypeDef *,int,float);
void Gyro_SetScale(SPI_HandleTypeDef *,int);

void Gyro_EnableHighPassFilter(SPI_HandleTypeDef *,uint8_t,float);

void Gyro_ReadRawData(SPI_HandleTypeDef *,uint8_t *);
void Gyro_Read(SPI_HandleTypeDef *,float *,float);

void Gyro_Degree1Step(float *,float *,float);

#endif /* L3GD20_H_ */
