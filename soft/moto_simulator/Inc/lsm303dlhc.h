/*
 * lsm303dlhc.h
 *
 *  Created on: 2 lut 2017
 *      Author: Olek
 */

#ifndef LSM303DLHC_H_
#define LSM303DLHC_H_

#include "stm32f3xx_hal.h"
#include <math.h>

// Addresses of devices
#define LSM303_ACC_ADDRESS (0x19 << 1)
#define LSM303_MAG_ADDRESS (0x1E << 1)

// Accelerometer - registers addresses
#define CTRL_REG1_A 0x20
#define CTRL_REG2_A 0x21
#define CTRL_REG3_A 0x22
#define CTRL_REG4_A 0x23
#define CTRL_REG5_A 0x24
#define CTRL_REG6_A 0x25
#define REFERENCE_A 0x26
#define STATUS_REG_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D
#define FIFO_CTRL_REG_A 0x2E
#define FIFO_SRC_REG_A 0x2F
#define INT1_CFG_A 0x30
#define INT1_SRC_A 0x31
#define INT1_THS_A 0x32
#define INT1_DURATION_A 0x33
#define INT2_CFG_A 0x34
#define INT2_SRC_A 0x35
#define INT2_THS_A 0x36
#define INT2_DURATION_A 0x37
#define CLICK_CFG_A 0x38
#define CLICK_SRC_A 0x39
#define CLICK_THS_A 0x3A
#define TIME_LIMIT_A 0x3B
#define TIME_LATENCY_A 0x3C
#define TIME_WINDOW_A 0x3D

// Magnetometer - registers addresses
#define CRA_REG_M 0x00
#define CRB_REG_M 0x01
#define MR_REG_M 0x02
#define OUT_X_H_M 0x03
#define OUT_X_L_M 0x04
#define OUT_Z_H_M 0x05
#define OUT_Z_L_M 0x06
#define OUT_Y_H_M 0x07
#define OUT_Y_L_M 0x08
#define SR_REG_M 0x09
#define IRA_REG_M 0x0A
#define IRB_REG_M 0x0B
#define IRC_REG_M 0x0C
#define TEMP_OUT_H_M 0x31
#define TEMP_OUT_L_M 0x32

// LSM303DLHC Struct
typedef struct
{
	float x;




} lsm303dlhc;


// [FUNCTIONS]

// Accelerometer
void Acc_Write1Byte(I2C_HandleTypeDef *,uint8_t,uint8_t *);
void Acc_WriteMultipleBytes(I2C_HandleTypeDef *,uint8_t,uint8_t *,int);
void Acc_Read1Byte(I2C_HandleTypeDef *,uint8_t,uint8_t *);
void Acc_ReadMultipleBytes(I2C_HandleTypeDef *,uint8_t,uint8_t *,int);

void Acc_Init(I2C_HandleTypeDef *,int);
void Acc_SetScale(I2C_HandleTypeDef *,uint8_t,uint8_t);

void Acc_EnableHighPassFilter(I2C_HandleTypeDef *,uint8_t,uint8_t,uint8_t);

void Acc_ReadRawData(I2C_HandleTypeDef *,uint8_t *);
void Acc_Read(I2C_HandleTypeDef *,float *,float);

void Acc_HighResEnable(I2C_HandleTypeDef *);
void Acc_HighResDisable(I2C_HandleTypeDef *);

void Acc_Degree1Step(float *,float *);

//void StartupExample(...);

// Magnetometer
void Mag_Write1Byte(I2C_HandleTypeDef *,uint8_t,uint8_t *);
void Mag_WriteMultipleBytes(I2C_HandleTypeDef *,uint8_t,uint8_t *,int);
void Mag_Read1Byte(I2C_HandleTypeDef *,uint8_t,uint8_t *);
void Mag_ReadMultipleBytes(I2C_HandleTypeDef *,uint8_t,uint8_t *,int);

void Mag_Init(I2C_HandleTypeDef *,float);
void Mag_SetRange(I2C_HandleTypeDef *,float);

void Mag_ReadRawData(I2C_HandleTypeDef *,uint8_t *);
void Mag_Read(I2C_HandleTypeDef *,float *,float);

void Mag_Degree1Step(float *,float *);

// TODO Funkcje do inicjalizacji wybranymi parametrami - zdecydowac jeszcze jak to rozwiazac
// TODO Funkcje do gotowego odczytu z akcelerometru i magnetometru do struktury
// TODO Funkcje do przekonwertowania bezposredniego odczytu na gotowe wyniki (float) - przemyslec
// TODO Co to MemAddSize w I2C ?!?
// TODO Mo�e p�niej porobic wersje z kom. DMA albo tylko wersja z kom DMA jak sie uda
// TODO ogarn�c FIFO i przerwania jakie� mo�e w przyszlosci

#endif /* LSM303DLHC_H_ */
