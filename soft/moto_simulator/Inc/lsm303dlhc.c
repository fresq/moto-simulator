/*
 * lsm303dlhc.c
 *
 *  Created on: 2 lut 2017
 *      Author: Olek
 */

#include "lsm303dlhc.h"


// Accelerometer functions


void Acc_Write1Byte(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t* data)
{
	HAL_I2C_Mem_Write(hi2c,LSM303_ACC_ADDRESS,reg_address,1,data,1,100);
}

void Acc_WriteMultipleBytes(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t *data,int size)
{
	reg_address |= 0x80;		// When MSB of register address is 1, addresses are auto-incremented in multiple read/write

	HAL_I2C_Mem_Write(hi2c,LSM303_ACC_ADDRESS,reg_address,1,data,size,100);
}

void Acc_Read1Byte(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t* data)
{
	HAL_I2C_Mem_Read(hi2c,LSM303_ACC_ADDRESS,reg_address,1,data,1,100);
}

void Acc_ReadMultipleBytes(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t *data,int size)
{
	reg_address |= 0x80;		// When MSB of register address is 1, addresses are auto-incremented in multiple read/write

	HAL_I2C_Mem_Read(hi2c,LSM303_ACC_ADDRESS,reg_address,1,data,size,100);
}

void Acc_Init(I2C_HandleTypeDef *hi2c,int data_rate)
{
	// data_rate -> [in Hz] -> 0,1,10,25,50,100,200,400,1620,1344 -> Default = 100Hz

	uint8_t settings=0x07;		// All axis enabled

	switch(data_rate)
	{
		case 0:
			//settings |= 0x00;
			break;
		case 1:
			settings |= 0x10;
			break;
		case 10:
			settings |= 0x20;
			break;
		case 25:
			settings |= 0x30;
			break;
		case 50:
			settings |= 0x40;
			break;
		case 100:
			settings |= 0x50;
			break;
		case 200:
			settings |= 0x60;
			break;
		case 400:
			settings |= 0x70;
			break;
		case 1620:
			settings |= 0x80;
			break;
		case 1344:
			settings |= 0x90;
			break;
		default:	// When data_rate isn't exact number from datasheet, rate is set to 100 Hz
			settings |= 0x50;
			break;
	}
	Acc_Write1Byte(hi2c,CTRL_REG1_A,&settings);
}

void Acc_SetScale(I2C_HandleTypeDef *hi2c,uint8_t scale,uint8_t high_res_mode)
{
	// scale -> [in g] -> 2,4,8,16 -> default = 2
	// high_res_mode -> 0 - high resolution mode disabled , 1 - high resolution mode enabled

	uint8_t settings=0x00;

	switch(scale)
	{
		case 2:
			//settings |= 0x00;
			break;
		case 4:
			settings |= 0x10;
			break;
		case 8:
			settings |= 0x20;
			break;
		case 16:
			settings |= 0x30;
			break;
		default:
			break;
	}

	if (high_res_mode != 0)
	{
		settings |= 0x08;
	}

	Acc_Write1Byte(hi2c,CTRL_REG4_A,&settings);
}


void Acc_EnableHighPassFilter(I2C_HandleTypeDef *hi2c,uint8_t mode,uint8_t frequency,uint8_t data_selection)
{
	// mode - 0,1,2,3
	// frequency - 0,1,2,3
	// data_selection - 0,1

	uint8_t settings=0x00;

	switch(mode)
	{
	case 0:
//		settings |= 0x00;
		break;
	case 1:
		settings |= 0x40;
		break;
	case 2:
		settings |= 0x80;
		break;
	case 3:
		settings |= 0xC0;
		break;
	}

	switch(frequency)
	{
	case 0:
//		settings |= 0x00;
		break;
	case 1:
		settings |= 0x10;
		break;
	case 2:
		settings |= 0x20;
		break;
	case 3:
		settings |= 0x30;
		break;
	}

	if (data_selection != 0)
	{
		settings |= 0x08;
	}

	Acc_Write1Byte(hi2c,CTRL_REG2_A,&settings);
}

void Acc_ReadRawData(I2C_HandleTypeDef *hi2c,uint8_t *data)
{
	// data in format [X_L][X_H][Y_L][Y_H][Z_L][Z_H]

	Acc_ReadMultipleBytes(hi2c,OUT_X_L_A,data,6);
}

void Acc_Read(I2C_HandleTypeDef *hi2c,float *accelerations,float scale)
{
	// accelerations in format [X][Y][Z]

	uint8_t data[6];
	int16_t data_processed[3];

	Acc_ReadRawData(hi2c,data);

	data_processed[0] = ((data[1] << 8) | data[0]);
	data_processed[1] = ((data[3] << 8) | data[2]);
	data_processed[2] = ((data[5] << 8) | data[4]);

	accelerations[0]=(float)data_processed[0]*scale/(float)INT16_MAX;
	accelerations[1]=(float)data_processed[1]*scale/(float)INT16_MAX;
	accelerations[2]=(float)data_processed[2]*scale/(float)INT16_MAX;
}


void Acc_HighResEnable(I2C_HandleTypeDef *hi2c)
{
	uint8_t settings=0x00;
	uint8_t mask=0x08;		// 1111 0111

	Acc_Read1Byte(hi2c,CTRL_REG4_A,&settings);

	settings |= mask;

	Acc_Write1Byte(hi2c,CTRL_REG4_A,&settings);
}



void Acc_HighResDisable(I2C_HandleTypeDef *hi2c)
{
	uint8_t settings=0x00;
	uint8_t mask=0xF7;		// 1111 0111

	Acc_Read1Byte(hi2c,CTRL_REG4_A,&settings);

	settings &= mask;

	Acc_Write1Byte(hi2c,CTRL_REG4_A,&settings);

}

void Acc_Degree1Step(float* acc_readings,float* acc_degrees)
{
	  acc_degrees[0]=atan2(acc_readings[1],acc_readings[2])*180/M_PI;
	  acc_degrees[1]=atan2(acc_readings[0],acc_readings[2])*180/M_PI;
	  // for z axis accelerometr is not sufficient
}




// Magnetometer functions

void Mag_Write1Byte(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t* data)
{
	HAL_I2C_Mem_Write(hi2c,LSM303_MAG_ADDRESS,reg_address,1,data,1,100);
}

void Mag_WriteMultipleBytes(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t *data,int size)
{
	reg_address |= 0x80;		// When MSB of register address is 1, addresses are auto-incremented in multiple read/write

	HAL_I2C_Mem_Write(hi2c,LSM303_MAG_ADDRESS,reg_address,1,data,size,100);
}

void Mag_Read1Byte(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t* data)
{
	HAL_I2C_Mem_Read(hi2c,LSM303_MAG_ADDRESS,reg_address,1,data,1,100);
}

void Mag_ReadMultipleBytes(I2C_HandleTypeDef *hi2c,uint8_t reg_address,uint8_t *data,int size)
{
	reg_address |= 0x80;		// When MSB of register address is 1, addresses are auto-incremented in multiple read/write

	HAL_I2C_Mem_Read(hi2c,LSM303_MAG_ADDRESS,reg_address,1,data,size,100);
}

void Mag_Init(I2C_HandleTypeDef *hi2c,float data_rate)
{
	// data_rate -> [in Hz] -> 0.7,1.5,3.0,7.5,15,30,75,220 -> Default = 75Hz

	uint8_t settings=0x00;

	data_rate = data_rate * 100;

	int sw_data_rate = (int) data_rate;

	switch (sw_data_rate)
	{
		case 75:
			//settings |= 0x00;
			break;
		case 150:
			settings |= 0x04;
			break;
		case 300:
			settings |= 0x08;
			break;
		case 750:
			settings |= 0x0C;
			break;
		case 1500:
			settings |= 0x10;
			break;
		case 3000:
			settings |= 0x14;
			break;
		case 7500:
			settings |= 0x18;
			break;
		case 22000:
			settings |= 0x1C;
			break;
		default:
			settings |= 0x18;		// sets to 75
			break;
	}

	Mag_Write1Byte(hi2c,CRA_REG_M,&settings);

	settings = 0x00;
	Mag_Write1Byte(hi2c,MR_REG_M,&settings);

}

void Mag_SetRange(I2C_HandleTypeDef *hi2c,float range)
{
	// range -> [in Gauss] -> 1.3,1.9,2.5,4.0,4.7,5.6,8.1 -> default = 4.0

	uint8_t settings = 0x00;

	range=range*10;

	int sw_range = (int) range;

	switch (sw_range)
	{
		case 13:
			settings |= 0x20;
			break;
		case 19:
			settings |= 0x40;
			break;
		case 25:
			settings |= 0x60;
			break;
		case 40:
			settings |= 0x80;
			break;
		case 47:
			settings |= 0xA0;
			break;
		case 56:
			settings |= 0xC0;
			break;
		case 81:
			settings |= 0xE0;
			break;
		default:
			settings |= 0x80;		// set to 4.0 by default
			break;
	}

	Mag_Write1Byte(hi2c,CRB_REG_M,&settings);

}

void Mag_ReadRawData(I2C_HandleTypeDef *hi2c,uint8_t *data)
{
	// data in format [X_H][X_L][Y_H][Y_L][Z_H][Z_L]

	uint8_t swapper;

	Mag_ReadMultipleBytes(hi2c,OUT_X_H_M,data,6);

	// changing order of data - in device data is stored as [X][Z][Y] -> changing to [X][Y][Z]
	swapper=data[2];
	data[2]=data[4];
	data[4]=swapper;

	swapper=data[3];
	data[3]=data[5];
	data[5]=swapper;
}


void Mag_Read(I2C_HandleTypeDef *hi2c,float *magnetisms,float range)
{
	// magnetisms in format [X][Y][Z]

	uint8_t data[6];
	int16_t data_processed[3];

	Mag_ReadRawData(hi2c,data);

	data_processed[0] = ((data[0] << 8) | data[1]);
	data_processed[1] = ((data[2] << 8) | data[3]);
	data_processed[2] = ((data[4] << 8) | data[5]);

	magnetisms[0]=(float)data_processed[0]*range/(float)2048;
	magnetisms[1]=(float)data_processed[1]*range/(float)2048;
	magnetisms[2]=(float)data_processed[2]*range/(float)2048;
}

void Mag_Degree1Step(float* mag_readings,float* mag_degrees)
{
	// Only for z axis
	mag_degrees[2]=atan2(mag_readings[1],mag_readings[0])*180/M_PI;
}

