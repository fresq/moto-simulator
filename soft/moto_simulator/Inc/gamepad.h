/*
 * gamepad.h
 *
 *  Created on: 26.06.2017
 *      Author: Tomek
 */

#ifndef GAMEPAD_H_
#define GAMEPAD_H_


	PROGMEM char usbHidReportDescriptor[51] = {
			0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
						0x09, 0x05,                    // USAGE (Game Pad)
						0xa1, 0x01,                    // COLLECTION (Application)
						0xa1, 0x00,                    //   COLLECTION (Physical)
						0x05, 0x09,                    //     USAGE_PAGE (Button)
						0x19, 0x01,                    //     USAGE_MINIMUM (Button 1)
						0x29, 0x10,                    //     USAGE_MAXIMUM (Button 16)
						0x15, 0x00,                    //     LOGICAL_MINIMUM (0)
						0x25, 0x01,                    //     LOGICAL_MAXIMUM (1)
						0x95, 0x10,                    //     REPORT_COUNT (16)
						0x75, 0x01,                    //     REPORT_SIZE (1)
						0x81, 0x02,                    //     INPUT (Data,Var,Abs)
						0x05, 0x01,                    //     USAGE_PAGE (Generic Desktop)
						0x09, 0x30,                    //     USAGE (X)
						0x09, 0x31,                    //     USAGE (Y)
						0x09, 0x32,                    //     USAGE (Z)
						0x09, 0x33,                    //     USAGE (Rx)
						0x15, 0x81,                    //     LOGICAL_MINIMUM (-127)
						0x25, 0x7f,                    //     LOGICAL_MAXIMUM (127)
						0x35, 0x00,                    //     PHYSICAL_MINIMUM (0)
						0x46, 0xff, 0x00,              //     PHYSICAL_MAXIMUM (255)
						0x75, 0x08,                    //     REPORT_SIZE (8)
						0x95, 0x04,                    //     REPORT_COUNT (4)
						0x81, 0x02,                    //     INPUT (Data,Var,Abs)
						0xc0,                          //     END_COLLECTION
						0xc0                           // END_COLLECTION
	};

	typedef struct
	{
		uint8_t report_id;
		uint8_t modifier;
		uint8_t reserved;
		uint8_t keycodes[6];
	} keyboard_report_t;

	typedef struct
	{
		uint8_t report_id;
		uint8_t buttons;
		int8_t x;
		int8_t y;
		int8_t wheel;
	} mouse_report_t;

	typedef struct
	{
		uint8_t report_id;
		uint16_t buttons;
		int8_t lx;
		int8_t ly;
		int8_t rx;
		int8_t ry;
	} gamepad_report_t;



	static keyboard_report_t keyboard_report;
	static mouse_report_t mouse_report;
	static gamepad_report_t gamepad_report_1;
	static gamepad_report_t gamepad_report_2;
	static keyboard_report_t keyboard_report_old;
	static mouse_report_t mouse_report_old;
	static gamepad_report_t gamepad_report_1_old;
	static gamepad_report_t gamepad_report_2_old;



#endif /* GAMEPAD_H_ */
