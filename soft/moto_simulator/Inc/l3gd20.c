/*
 * l3gd20.c
 *
 *  Created on: 2 lut 2017
 *      Author: Olek
 */

#include "l3gd20.h"

// Gyroscope functions

void Gyro_Write1Byte(SPI_HandleTypeDef *hspi,uint8_t reg_address,uint8_t *data)
{

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi,&reg_address,1,100);
	HAL_SPI_Transmit(hspi,data,1,100);

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_SET);
}

void Gyro_WriteMultipleBytes(SPI_HandleTypeDef *hspi,uint8_t reg_address,uint8_t *data,int size)
{
	reg_address |= 0x40;			// enables auto-incrementation of addresses

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi,&reg_address,1,100);
	HAL_SPI_Transmit(hspi,data,size,100);

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_SET);
}

void Gyro_Read1Byte(SPI_HandleTypeDef *hspi,uint8_t reg_address,uint8_t *data)
{
	uint8_t trash[1];
	reg_address |= 0x80;

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi,&reg_address,1,100);
	HAL_SPI_Receive(hspi,trash,1,100);			// Reading trash from buffer [?]
	HAL_SPI_Receive(hspi,data,1,100);

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_SET);
}

void Gyro_ReadMultipleBytes(SPI_HandleTypeDef *hspi,uint8_t reg_address,uint8_t *data,int size)
{
	uint8_t trash[1];
	reg_address |= 0x80;			// read bit

	reg_address |= 0x40;			// enables auto-incrementation of addresses

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi,&reg_address,1,100);
	HAL_SPI_Receive(hspi,trash,1,100);			// Reading trash from buffer [?]
	HAL_SPI_Receive(hspi,data,size,100);

	HAL_GPIO_WritePin(SS_PORT,SS_PIN,GPIO_PIN_SET);
}


void Gyro_Init(SPI_HandleTypeDef *hspi,int data_rate,float bandwidth)
{
	uint8_t settings = 0x0F;		// Power-on mode and all axis enabled

	switch (data_rate)
	{
		case 95:
//			settings |= 0x00;
			if (bandwidth == 12.5)
				settings |= 0x00;
			else if (bandwidth == 25)
				settings |= 0x10;
			else
				settings |= 0x10;
			break;

		case 190:
			settings |= 0x40;

			if (bandwidth == 12.5)
				settings |= 0x00;
			else if (bandwidth == 25)
				settings |= 0x10;
			else if (bandwidth == 50)
				settings |= 0x20;
			else if (bandwidth == 70)
				settings |= 0x30;
			else
				settings |= 0x10;

			break;

		case 380:
			settings |= 0x80;

			if (bandwidth == 20)
				settings |= 0x00;
			else if (bandwidth == 25)
				settings |= 0x10;
			else if (bandwidth == 50)
				settings |= 0x20;
			else if (bandwidth == 100)
				settings |= 0x30;
			else
				settings |= 0x10;

			break;

		case 760:
			settings |= 0xC0;

			if (bandwidth == 30)
				settings |= 0x00;
			else if (bandwidth == 35)
				settings |= 0x10;
			else if (bandwidth == 50)
				settings |= 0x20;
			else if (bandwidth == 100)
				settings |= 0x30;
			else
				settings |= 0x10;

			break;
	}
	Gyro_Write1Byte(hspi,CTRL_REG1,&settings);
	settings = 0x20;
	Gyro_Write1Byte(hspi,CTRL_REG2,&settings);
}

void Gyro_SetScale(SPI_HandleTypeDef *hspi,int scale)
{
	// scale -> [in dps] -> 250,500,2000

	uint8_t settings = 0x00;

	switch (scale)
	{
		case 250:
			settings |= 0x00;
			break;
		case 500:
			settings |= 0x10;
			break;
		case 2000:
			settings |= 0x20;
			break;
		default:			// Default = 500 dps
			settings |= 0x10;
			break;
	}

	Gyro_Write1Byte(hspi,CTRL_REG4,&settings);

}

void Gyro_ReadRawData(SPI_HandleTypeDef *hspi,uint8_t *data)
{
	// data in format [X_L][X_H][Y_L][Y_H][Z_L][Z_H]

//	Gyro_ReadMultipleBytes(hspi,OUT_X_L,data,6);
	Gyro_Read1Byte(hspi,OUT_X_L,&data[0]);
	Gyro_Read1Byte(hspi,OUT_X_H,&data[1]);
	Gyro_Read1Byte(hspi,OUT_Y_L,&data[2]);
	Gyro_Read1Byte(hspi,OUT_Y_H,&data[3]);
	Gyro_Read1Byte(hspi,OUT_Z_L,&data[4]);
	Gyro_Read1Byte(hspi,OUT_Z_H,&data[5]);
}


void Gyro_Read(SPI_HandleTypeDef *hspi,float *velocities,float scale)
{
	uint8_t data[6];
	int16_t data_processed[3];

	Gyro_ReadRawData(hspi,data);

	data_processed[0] = ((data[1] << 8) | data[0]);
	data_processed[1] = ((data[3] << 8) | data[2]);
	data_processed[2] = ((data[5] << 8) | data[4]);

	velocities[0]=(float)data_processed[0]*scale/(float)INT16_MAX;
	velocities[1]=(float)data_processed[1]*scale/(float)INT16_MAX;
	velocities[2]=(float)data_processed[2]*scale/(float)INT16_MAX;
}

void Gyro_Degree1Step(float* gyro_readings,float* gyro_degrees,float dt)
{
	// dt - period

	gyro_degrees[0]+=gyro_readings[0]*dt;
	gyro_degrees[1]+=gyro_readings[1]*dt;
	gyro_degrees[2]+=gyro_readings[2]*dt;
}
